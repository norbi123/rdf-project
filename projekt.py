import rdflib
import csv
from rdflib import Graph

g= Graph()
g2= Graph()
gatunkiFilmowe = []
filmy = [[[] for i in range(23)] for i in range(1)]
oceny = [[[] for i in range(4)] for i in range(1)]

#zczytanie mozliwych gatunkow filmowych
with open('gatunki.csv', 'rt') as f:
	reader = csv.reader(f, delimiter='|')
	for row in reader:
		gatunkiFilmowe.insert(int(row[1]), row[0])

# wyznaczenie ilosci lini w pliku filmy
with open('filmy.csv', 'rt') as f:
	reader = csv.reader(f, delimiter='|')
	rowNumber = 0
	for row in reader:
		rowNumber += 1
	filmy = [[[] for i in range(23)] for i in range(rowNumber)]
	
# wyznaczenie ilosci lini w pliku oceny
with open('oceny.csv', 'rt') as f:
	reader = csv.reader(f, delimiter='|')
	rowNumber = 0
	for row in reader:
		rowNumber += 1
	oceny = [[[] for i in range(4)] for i in range(rowNumber)]

#csv -> list filmy
with open('filmy.csv', 'rt') as f:
	reader = csv.reader(f, delimiter='|')
	i=0
	for row in reader:
		filmy[i] = [row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10],
		row[11], row[12], row[13], row[14], row[15], row[16], row[17], row[18], row[19], row[20], row[21], row[22], row[23]]
		i += 1
		
#csv -> list oceny
with open('oceny.csv', 'rt') as f:
	reader = csv.reader(f, delimiter='	')
	i=0
	for row in reader:
		oceny[i] = [row[0], row[1], row[2], row[3]]
		i += 1
		
#dodawanie S P O, P = 'ma gatunek'
for elem in filmy:
	for x in range(5,23):
		if int(elem[x]):
			g.add((elem[1], 'ma gatunek', gatunkiFilmowe[x-5]))
			
#dodawanie S P O, P = 'lubi/nie lubi'
for elem in oceny:
	if int(elem[2]) == 1 or int(elem[2]) == 2:
			g2.add(("user" + elem[0], 'nie lubi', filmy[int(elem[1])-1][1]))
	else:
			g2.add(("user" + elem[0], 'lubi', filmy[int(elem[1])-1][1]))

for triple in g:
	print(triple)
	
for triple in g2:
	print(triple)